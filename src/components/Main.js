import React, { Component } from 'react';
import './Main.css';
import Header from './Header';
import Noticias from './Noticias';
import Footer from './Footer';
import { API_URL, API_COUNTRY, API_TOKEN } from '../config';
export default class Main extends Component {
	state = {
		noticias: []
	};

	componentDidMount() {
		this.getNoticias();
	}

	getNoticias = async (categoria = 'general') => {
		const URL = `${API_URL}/top-headlines?country=${API_COUNTRY}&category=${categoria}&apiKey=${API_TOKEN}`;
		await fetch(URL)
			.then(resp => resp.json())
			.then(data => this.setState({ noticias: data.articles }))
			.catch(error => console.log(error));
	};

	render() {
		const { noticias } = this.state;
		return (
			<React.Fragment>
				<div className="container mt-3">
					<Header getNoticias={this.getNoticias} />
					<Noticias noticias={noticias} />
					<Footer />
				</div>
			</React.Fragment>
		);
	}
}
