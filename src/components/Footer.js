import React from 'react';

export default function Footer() {
	return (
		<footer className="blog-footer">
			<p>
				Blog template built for <a href="/">Giovanny</a> by <a href="/">@gio</a>.
			</p>
		</footer>
	);
}
