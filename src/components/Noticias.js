import React from 'react';
import Noticia from './Noticia';

export default function Noticias({ noticias }) {
	return <div className="row mb-2">{noticias.map((noticia, i) => <Noticia key={i} noticia={noticia} />)}</div>;
}
