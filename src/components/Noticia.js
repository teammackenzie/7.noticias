import React from 'react';

export default function Noticia({ noticia }) {
	const { title, url, urlToImage, source, publishedAt, content } = noticia;
	const newImagen = urlToImage
		? urlToImage
		: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7IDT7utX0dRl4FNp9QoovBKnFz0b9UZuMWo8D-CBb4SsSBY9c';
	return (
		<div className="col-md-6">
			<div className="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
				<div className="card">
					<img src={newImagen} className="card-img-top img-noticias img-fluid" alt={source.name} />
					<small className="text-muted">{publishedAt}</small>
					<div className="card-body">
						<h5 className="card-title">{title}</h5>
						<p className="card-text">{content}</p>
						<a href={url} className="btn btn-outline-dark btn-sm" target="_blank" rel="noopener noreferrer">
							{source.name}
						</a>
					</div>
				</div>
			</div>
		</div>
	);
}
